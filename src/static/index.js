import Vue from 'vue'
import App from './App'

new Vue({
  el: '#app',
  mode: 'production',
  render: h => h(App)
})