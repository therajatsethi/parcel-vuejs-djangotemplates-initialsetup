There are various ways to integrate Vue within Django.

# Basic Way (No Build Tools, just django templates) - Brief summary.

- Just include Vue CDN as in scripts tag withing your index.html pages wherever required.
- Declare a `<div id="app"></div>` in your django html template and pass data to it. Example given below.


```html
<!-- File DjangoTemplate.html -->


{% block maincontent %}

<!-- Passing data to javascript variables from django templates. -->

<script type='text/javascript'>
    var jsonData = {{ schedule|safe }};
    var jsonDataPeriods = {{ periods|safe }};
    var jsonDataSubjects = {{ subjects|safe }};
</script>

<div id="vueAppSpecific">
    [[schedule]]  <!-- Custom View Delimeters. -->
</div>


<!-- Vue Instance -->
<script>
    var vueAppSpecific = new Vue({

    el: '#vueAppSpecific',
    delimiters: ['[[', ']]'],

<!-- Store the JS variables in vue instance and then use them in template as shown above. -->

    data : {
        schedule: jsonData,
        jsonDataPeriods: jsonDataPeriods,
        jsonDataSubjects: jsonDataSubjects,
    },

    methods: {
    },

    computed: {
    },

});

</script>
</div>
{% endblock %}
```

---

# Using FrontEnd Build Tools (This method will use Vue Components)

- Setup the build tools as shown in repo.
- The benefits are that you can use Vue components and then the JS can be compiled down. However you cannot pass data from Django template to Vue as shown in the above approach as the HTML is not within Django html file but its in .Vue file which is compiled to JS before django is importing it.
- Use the API (DRF) to get data to Vue. (Use django session auth to keep it simple.)
- Use parcel to complie the Index.js to dist folder with same name `parcel watch index.js --out-file bundle.js`
- Include this bundle file in Django just like any other JS file. Parcel will minimize and optimize this.
- While development you will be running 2 servers. 1) Django runserver 2) parcel watch server for VueJS.
- So some of the part of your app will be Django's template controlled and some will be Vue controlled.
- In `index.js` you can create multiple Vue components and use them in different Django html pages to keep things seperate.